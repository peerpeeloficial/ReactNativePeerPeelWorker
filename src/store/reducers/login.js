import {GET_API_USER} from '../types/';

const initialState = {
    user:'',
    pass:'',
    token:''
}

const user = (state=initialState, action)=>{
    switch(action.type){
        
        case GET_API_USER:
        return {...state,user : action.payload}//porque se utiliza payload, cual es su significado
        //porque se le llama user
        default:
        return state
    }
}
export default user;