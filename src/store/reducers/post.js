//import {fromJS} from 'immutable';
//TYPES
import { GET_API_POST, ON_SELECT } from '../types/';

const initialState = //romJS(
{
    post: [],
    select: {}//aqui se define los estados iniciales
}
//) 
const post = (state = initialState, action) => {

    switch (action.type) {

        case GET_API_POST:
            return { ...state, post: action.payload }

        case ON_SELECT:
            return { ...state, select: action.payload }//esto como tal que hace

        default:
            return state
    }


}

export default post;