import { GET_API_POST, ON_SELECT } from '../types/';
import { Platform, StyleSheet, Text, View, AsyncStorage } from 'react-native';

export const getApiPost = (payload) => {

    return {
        type: GET_API_POST,
        payload
    }
}

export const onSelect =(payload)=>{
    return{
        type:ON_SELECT,
        payload
    }
}

//esto como tal que hace

export const getApiPostAsync = () => (dispatch) => {

    return AsyncStorage.getItem('token')
        .then(res => {
            console.log(res,'as')
            return fetch('http://31.220.55.37:8080/api/post/fetchAll', {
                method: 'GET',
                headers: { 'Authorization': res },
            }).then((res) => res.json())
                .then(res => {
                    dispatch(getApiPost(res.response))
                }).catch((e) => {
                    console.log(e)
                })
        })
}
