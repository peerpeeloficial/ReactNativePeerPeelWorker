import React, { PureComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { connect } from 'react-redux';

// importacion de componentes
import Boton from '../../components/button/button';
import Texto from '../../components/text/text';
 class SeeMore extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    takeOrder=()=>{
        console.warn('tomando pedido');
    }


    render() {
        let{ description,price, title} = this.props.select
        let {name, lastname,  email,phone, direction}= this.props.select.user

        
        console.log(this.props.select)
        //   let {user,pass}=this.state;
        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <View style={styles.containerDescription}>

                    <View style={{backgroundColor:'white', width:'95%',alignItems:'center', borderRadius:10}}>

                        <Text style={styles.textTitle}>{title}</Text>
                        <Text style={[styles.description,{textAlign:'auto',margin:10, }]}>{description}</Text>
                    </View>
                    
                    </View>
                    <View style={styles.containerData}>
                    <View style={{backgroundColor:'white', width:'95%',justifyContent:'center', borderRadius:10,}}>
                        <Text style={[styles.textTitle,{color:'red', textAlign:'center', }]}>Datos del cliente</Text>
                        <View style={styles.containerDataCliente}>
                            <Text style={styles.textTitle}>{`Direccion: `}</Text>
                        <Text>{direction}</Text>
                            </View>
                            <View style={styles.containerDataCliente}>
                        <Text style={styles.textTitle}>{`Nombre: `}</Text>
                        <Text>{`${name} ${lastname}`}</Text>

                    </View>
                    <View style={styles.containerDataCliente}>
                        <Text style={styles.textTitle}>{`Celular: `}</Text>
                        <Text>{phone}</Text>
                        </View>
                        <View style={styles.containerDataCliente}>
                        <Text style={styles.textTitle}>{`Corrreo: `}</Text>
                        <Text>{email}</Text>
                    </View>
                    <View style={styles.containerDataCliente}>
                        <Text style={styles.textTitle}>{`Precio: `}</Text>
                        <Text>{price}</Text>
                        </View>


                    </View>

                    </View>
                    <View style={styles.containerButton}>
                        <Boton
                            handleSubmit={this.takeOrder}
                        >Tomar servicio</Boton>
                    </View>
                </View>
            </KeyboardAwareScrollView>

        );
    }
}
//https://github.com/edupooch/simple-crna-routing
const styles = StyleSheet.create({
    container: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        backgroundColor: 'skyblue',
        alignItems: 'center'
    },
    containerDescription: {
        height: '40%',
        width: '90%',
        borderRadius:10,
        // justifyContent: 'space-around',
        alignItems: 'center',
        justifyContent:'center'
    },
    containerData: {
        borderRadius:10,
        width: '85%',
        height: '20%',
         backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerButton: {
        height: '30%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    textTitle: {
        fontWeight: 'bold',
        fontSize: 15,
        fontFamily: 'sans-serif',
        color: 'black'
    },
    containerDataCliente:{
        flexDirection:'row',
        alignItems: 'center',
        borderRadius:10,
    },
    description:{
        textAlign:'justify'
    }


});

const mapStateToProps = (state,props) => {
    console.log(state.post)
    return {
        select:state.post.select
    } 
}
// const mapDispatchToProps = {
//     getApiPostAsync,
//     onSelect
// }

export default connect(mapStateToProps)(SeeMore)
