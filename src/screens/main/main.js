import React, { Component } from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import {
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";

import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import { getApiPostAsync,onSelect } from '../../store/actions/post';

// importacion de componentes
import CardPost from '../../components/cardPost/cardPost';

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }
    handleSubmit = (item) => {
        console.log('SeeMore')
        this.props.onSelect(item)
        Actions.seeMore();
    }

    componentWillMount() {
        // this.showToken();
        this.props.getApiPostAsync();
    }

    render() {
        const Posts = this.props.post.map((item, index) => {
            console.log(item.description)
            return (
                <CardPost
                    key={index}
                    name={item.title.toUpperCase()}
                    onHandle={() => this.handleSubmit(item)} />
            )
        }
        )
        return (
            
            <View style={styles.container}>
                <ScrollView>
                <View style={styles.bodyContainer}>
                    {Posts}
                </View >
            </ScrollView>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: responsiveHeight(88),
        width: responsiveWidth(100),
        backgroundColor: '#F5FCFF',
        alignItems: 'center',

    },
    bodyContainer: {
        flex: 1,
        alignItems: 'center',
        height: '30%',
        width: '100%'
    },
    containerHeader: {

        justifyContent: 'flex-start'

    },
    containerFooter: {
     
        height: responsiveHeight(85),
        width: responsiveWidth(100),
        justifyContent: 'flex-end'

    }
});
const mapStateToProps = (state,props) => {
    console.log(state.post)
    return {
        post:state.post.post
    } 
}
const mapDispatchToProps = {
    getApiPostAsync,
    onSelect
}
export default connect(mapStateToProps, mapDispatchToProps)(Main)