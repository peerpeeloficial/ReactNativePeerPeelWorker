/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import {
  responsiveFontSize,
  responsiveHeight
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/FontAwesome";
import { Footer, Button, FooterTab } from "native-base";

const iconSize = 4;

export default class MainTabBar extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Footer style={styles.container}>
        <FooterTab style={styles.footerTabContainer}>
          <Button onPress={()=>console.warn('Chat')}>
            <Icon name={"comments"} size={responsiveFontSize(iconSize)} />
            <Text style={styles.textStyle}>{"Chat"}</Text>
          </Button>
          <Button onPress={()=>console.warn('Post')}>
            <Icon name={"credit-card"} size={responsiveFontSize(iconSize)} />
            <Text style={styles.textStyle}>{"Post"}</Text>
          </Button>
          <Button onPress={()=>console.warn('Ajustes')}>
            <Icon name={"cog"} size={responsiveFontSize(iconSize)} />
            <Text style={styles.textStyle}>{"Ajustes"}</Text>
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: responsiveHeight(12),
    padding: 10,
    alignItems: "center",
    backgroundColor: 'orange',
    marginBottom:0
  },
  footerTabContainer: {
    width: "100%",
    height: "100%",
    backgroundColor: 'orange'
  },
  buttonStyle: {
    width: responsiveFontSize(6),
    height: responsiveFontSize(6),
    alignSelf: "flex-end",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: 'gray'
  },
  textStyle: {
    fontSize: responsiveFontSize(1.6),
    fontWeight: "normal",
    color: 'blue'
  }
});
