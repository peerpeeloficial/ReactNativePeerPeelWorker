import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ToastAndroid, ActivityIndicator } from 'react-native';
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
} from "react-native-responsive-dimensions";
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


import Helpers from '../../lib/helpers';
// importacion de componentes
import Input from '../../components/input/input';
import Boton from '../../components/button/button';
import Texto from '../../components/text/text';
export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: 'Julian@hotmail.com',
            pass: '123',
            isShow: false,
        }
    }

    handleInputChange = input => (ev) => {
        console.log(input)
        console.log(ev);
        this.setState({
            [input]: ev
        })
        console.log(this.state.user, this.state.pass)
    }

    handleSubmit = async () => {
        let { user, pass, isShow } = this.state;
        this.setState({ isShow: true })
        console.warn(user, pass);
        if (user && pass) {
            Helpers.handleSubmitLogin(this.state.user, this.state.pass)
                .then(res => {
                    if (res == 200) {
                        setTimeout(() => {

                            this.setState({ isShow: false })
                            Actions.main('')

                        }, 3000);
                    } else {
                        console.warn('datos incorrectos');
                        setTimeout(() => {

                        this.setState({ isShow: false })

                        ToastAndroid.showWithGravityAndOffset(
                            `Datos incorrectos`,
                            ToastAndroid.LONG,
                            ToastAndroid.BOTTOM,
                            25,
                            50
                        );
                    }, 3000);

                    }
                })
        } else {
            ToastAndroid.showWithGravityAndOffset(
                `debes llenar todos los campos`,
                ToastAndroid.LONG,
                ToastAndroid.BOTTOM,
                25,
                50
            );
        }
    }

    toRegistro = () => {
        try {
            console.warn('enviando a registro');
            Actions.registry();
        } catch (e) {
            console.log(e)
        }

    }


    render() {
        return (
            <KeyboardAwareScrollView>
                {this.state.isShow ? 
                <View style={styles.container}>
                    <ActivityIndicator size="large" color="red" />
                    <Text>Consultando la base de datos</Text>
                </View> :

                    <View style={styles.container}>
                        <View style={styles.containerForm}>
                            <View style={styles.imageContainer}>
                                {/* imagen */}
                                <Image
                                    source={require('../../assets/logo1.png')}
                                    style={{ width: 130, height: 130, risizeMode: 'contain' }}
                                />
                            </View>
                            <View style={styles.inputContainer}>
                                {/* Usuario y Login */}
                                <Input
                                    handleChange={this.handleInputChange('user')}
                                    value={this.state.user}
                                    tipo="usuario"
                                    typeKeyBoard="email-address"
                                    icon="envelope"
                                />
                                <Input
                                    handleChange={this.handleInputChange('pass')}
                                    value={this.state.pass}
                                    tipo="contraseña"
                                    secureText={true}
                                    icon="key"
                                />
                            </View>
                            <View style={styles.buttonContainer}>
                                <Boton
                                    handleSubmit={this.handleSubmit}
                                >Login</Boton>
                                <Texto
                                    handleTextRegistro={this.toRegistro}
                                >No tienes cuenta, registrate</Texto>

                            </View>

                        </View>
                    </View>
                }
            </KeyboardAwareScrollView>

        );
    }
}
//https://github.com/edupooch/simple-crna-routing
const styles = StyleSheet.create({
    container: {
        height: responsiveHeight(100),
        width: responsiveWidth(100),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00bfff',
    },
    containerForm: {
        height: responsiveHeight(70),
        width: responsiveWidth(90),
        backgroundColor: '#F5FCFF',
        borderRadius: 10,
    },
    imageContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainer: {
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    buttonContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }

});
