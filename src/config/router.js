import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View,Image,AsyncStorage,TouchableOpacity } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import Icon from "react-native-vector-icons/FontAwesome";
import {createStore, applyMiddleware} from 'redux';
import {Provider}from 'react-redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
//import {Map as map} from 'immutable';

import reducer from '../store/reducers/';

import Splash from '../screens/splash/splash';

import Login from '../screens/login/login';
import Registry from '../screens/registry/registry';
import Main from '../screens/main/main';
import SeeMore from '../screens/seeMore/seeMore';
import MainTabBar from '../screens/tabBar/MainTabBar';

const logger_personal = ({dispatch, getState}) => next => action =>{
  console.log('estado anterios: ', getState())
  console.log('enviando accion: ', action)
  const result  = next(action)
  console.log('nueva accion: ', getState())
  return result
}

const store = createStore(reducer,{},applyMiddleware(logger_personal,logger, thunk));
const Exit=() => (
  <TouchableOpacity
    onPress={()=>{
      AsyncStorage.clear()
      Actions.login()
    }
  }
  >
  <Icon name="sign-out" size={35} style={{rotation:180}}/>
  </TouchableOpacity>
)

export default class Rutes extends React.Component {
  render() {

    return (
      <Provider store={store}>
      <Router>
        <Scene key="root">
          <Scene
            key="splash"
            component={Splash}
            hideNavBar />
          <Scene
            key="login"
            component={Login}
            hideNavBar />
          <Scene key="registry"
            component={Registry}
            title="Registro" />

         <Scene
            key="main"
            component={Main}
            title="Servicios disponibles"
            onExit={()=>AsyncStorage.clear()}
           renderBackButton={()=><Exit/>}
           />
             <Scene
            key="seeMore"
            component={SeeMore}
            title="Detalles"
            />
        </Scene>
      </Router>
      </Provider>
    );
  }
}