import React from 'react';
import { TouchableOpacity,Text} from 'react-native';
const Boton = (props) => {
  return (
        <TouchableOpacity
        onPress={props.handleSubmit}
        style={{backgroundColor:'#ff5100', width:'80%',justifyContent:'center', alignItems:'center',height:60,borderRadius:10, fontWeight: 'bold'}}
        >
            <Text style={{fontWeight: 'bold',color:'white'}}>{props.children}</Text>
        </TouchableOpacity>
  )
}

export default Boton;