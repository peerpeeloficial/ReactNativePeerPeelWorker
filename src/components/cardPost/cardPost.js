import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, } from "react-native";
import {
    responsiveFontSize,
    responsiveHeight,
    responsiveWidth
  } from "react-native-responsive-dimensions";


const iconSize = 2.7;
const CardPost = props => (
    <TouchableOpacity 
        style={styles.card}
        onPress={props.onHandle}
    >
       
        <View style={styles.datos}>
        <Text style={{fontWeight:'bold'}}>{props.name}</Text>
        <TouchableOpacity
        onPress={props.onHandle}
        style={{height:30, width:'80%', borderRadius:5, backgroundColor:'#ff5100', justifyContent:'center', alignItems:'center', marginTop:10}}
        >
        <Text style={{color:'white'}}>Ver mas</Text>
        </TouchableOpacity>
        </View>
  
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    card: {
        width: responsiveWidth(70),
        height: 130,
        borderRadius: 10,
        backgroundColor: 'orange',
        marginTop:20,
        flexDirection: 'column',
        justifyContent:'space-around',

        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        marginBottom: 2

    },
    datos: {
    //   backgroundColor: 'blue',
        flex: 6,
        justifyContent:'center',
        alignItems:'center'
    },
    delete: {
          backgroundColor: 'green',
        flex: 1,
        alignItems:'center',
        marginTop:20
        },
    text:{
        fontWeight:'bold'
    }
});

export default CardPost;
