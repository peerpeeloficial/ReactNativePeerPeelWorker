import React, { Component } from 'react';

import { Actions } from 'react-native-router-flux';

import {
    AsyncStorage,
    ToastAndroid,
} from 'react-native';
class Helpers {

    static handleSubmitLogin = async (user, pass) => {
        console.log('helper')
        try {
            console.log('epa');
            return fetch(`http://31.220.55.37:8080/api/user/login`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: user,
                    password: pass
                }),
            }
            ).then((res) => {
                return res.json()
            }).then(json => {
                console.log(json.status);
                console.log(json.token)
                AsyncStorage.setItem('token',json.token);
                //(json.status == 200) ? Actions.main('') : console.log('datos incorrectos')
                return json.status
            }
                , )
                .catch((error) => console.log(error))
        } catch (e) {
            console.log(e)
        }
    }

    static handleSubmitRegistry = async (state) => {
        console.log('handle')
        console.log(state)
        try {
            return fetch(`http://31.220.55.37:8080/api/user/`, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: state.name,
                    lastname: state.lastName,
                    identification: state.id,
                    direction: state.direction,
                    phone: state.tel,
                    email: state.email,
                    password: state.pass
                }),
            }
            ).then((res) => {
                // console.log(res);
                // console.log('entro');
                // console.log(res._bodyInit);
                // console.log(typeof res._bodyInit);
                Actions.login('');
            })
                .catch((error) => console.log(error))
        } catch (error) {
            console.log(error)
        }
    }

}
export default Helpers;
